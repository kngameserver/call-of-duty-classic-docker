# Call of Duty 'Classic' Docker Image!

Here you get a small (~ 30MB) Docker Image for the Game Call of Duty & Call of Duty: United Offensive. You can host easily multiple gameserver instances with one gamedir! 


## 📦 What you get

 - Minimal Alpine Based Image
 - Well documented [Game Configurations](https://github.com/KeinNerd/docker-codclassic/tree/main/config)
 - Multiple Versions (Classic CoD: `1.4`, `1.5`, `1.5b` | CoD:UO `1.51`)
 - Download latest Serverfiles (you only need some files from your gamedir!)
 - Also "cracked" binaries so any user can join!

## 🚀 How to start
For a fast start simply 

     docker create \
      --name codclassic \
      -p 20500-20510:20500-20510/udp \
      -p 28960:28960 \
      -p 28960:28960/udp \
      -v <your/path/to/game/>:/gameserver \
      -v <your/path/to/game/>:/config \
      -e COD_VERSION="1_5b" \
      -e COD_GAME="main" \
      -e PUNKBUSTER="1" \
      -e STARTUP="+set fs_homepath config/ +set dedicated 2 +exec dedicated.cfg" \
    keinnerd/codclassic:latest

***Attention:*** 

 - Upload all `pak*.pk3` from your Retail / Steam `main` game dir into the servers `main`dir!
 - Upload all `pakuo*.pk3` from your Retail / Steam `uo` game dir into the servers `uo`dir!

## 🔧 Envs

|Env             |ASCII                          |HTML                         |
|----------------|-------------------------------|-----------------------------|
|COD_VERSION     | CoD: `1_4` or `1_5` or `1_5b` / UO: `1_51` |For cracked add a `_cracked` behind the Version|
|COD_GAME        |`main` or `uo` |`main` = Call of Duty / `uo` = Call of Duty: UO|
|PUNKBUSTER      |`1` or `0` |`enable` or `disable` Punkbuster (*1)|
|STARTUP         | "`+set fs_homepath config/ +set dedicated 2 +exec dedicated.cfg`" | Adjust your Start command (fs_basepath is hardcoded to /gameserver)

## 📁 Multiple Servers

If you want to host multiple servers, create a new container then create inside your config folder different dirs and change `+set fs_homepath config/` to `config/server1` the next to `config/server1`. Create Configs inside the dirs and spin up the container. 

```
── /config
 ├──> server1 (e.g call of duty)
 ├──────> dedicated.cfg
 ├──────> punkbuster.cfg
 ├──> server2 (e.g call of duty UO)
 ├──────> dedicateduo.cfg
 ├──────> punkbuster.cfg
 ├──> server3 (e.g AWEMod)
 ├──────> dedicated.cfg & aweconfig.cfg
 └──────> punkbuster.cfg
```

(*)1: If you enable Punkbuster a `punkbuster.cfg` will be copied in the `config` dir. To enable comment the Punkbuster vars in your dedicated(uo).cfg out!

Open needed Ports in your firewall! Default Ports: 20500/udp 20510/udp 28960/tcp 28960/udp

## 👾 Mods

If you want to play a Modification you need to upload your mod in the `gamedir` and adjust some ENV's

- Change `STARTUP` to `+set fs_game yourmod +set fs_homepath config/ +set dedicated 2 +exec dedicated.cfg` 


## 📝 ToDo

 - ~~Simple and easy mod support~~
 - Add [Codextended](https://github.com/riicchhaarrd/codextended) support

## 👐 Contribution

Feel free to fork and make pull requests. This are my first attempts with creating docker images so please give me feedback. 

## ⏰ Changelog

### 05.05.2021 
- Rebase to new [BaseImage](https://github.com/KeinNerd/docker-idtech-baseimage)
- Mod support
- Better multiserver support

### 09.04.2021 
- First public release
