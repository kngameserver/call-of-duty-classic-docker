# Runtime stage
FROM keinnerd/idtech-baseimage:latest

LABEL maintainer="fistwho@keinnerd.net"

ENV USER="docker"
ENV UID=1000
ENV GID=1000
ENV COD_VERSION="1_5b"
ENV COD_GAME="main"
ENV PUNKBUSTER="1"
ENV STARTUP="+set dedicated 2 +set fs_homepath config/ +exec dedicated.cfg"
ENV BUNDLE="kn-codclassic-large.tar"


# Copy needed libraries and binaries
RUN apk add --no-cache wget
COPY lib/pb/v1.729 /lib/${USER}/v1.729
COPY bin/* /bin/
COPY config/* /opt/config/
COPY ./entrypoint.sh /opt/entrypoint.sh
RUN chmod +x /opt/entrypoint.sh

# Server gamefiles folder and config volume
VOLUME [ "/gameserver", "/config" ]

# Set the server dir
WORKDIR /gameserver

# Launch server at container startup
ENTRYPOINT ["/opt/entrypoint.sh"]
