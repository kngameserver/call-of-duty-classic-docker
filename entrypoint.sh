#!/bin/sh

# Download basic linux serverfiles for Call of Duty and Call of Duty: United Offensive
if [ ! -d "/gameserver/main/" ] || [ ! -d "/gameserver/uo/" ]; then
    echo "---| Download Linux basic Serverfiles for Call of Duty                      |---"
    echo "---| after download you must upload your pak*.pk3 files from your game dir! |---"
    echo "---| Please Upload and restart the container, putting server into sleep mode|---"
    wait 3
    cd /gameserver
    if wget -nv --show-progress --progress=bar:force:noscroll "https://dl.keinnerd.net/cod/${BUNDLE}" ; then
                 echo "---| Successfully downloaded, now extracting |---"
         wait 2
         tar -xvf ${BUNDLE} -C /gameserver/
         rm ${BUNDLE}
         echo "---| Download finished |---"
         wait 3
            else
                 echo "---| Can't download '${BUNDLE}', putting server into sleep mode... |---"
         sleep infinity
        fi
fi

# Check for Gamefiles and download
if [ ! -f "/gameserver/main/pak1.pk3" ] || [ ! -f "/gameserver/uo/pakuo01.pk3" ]; then
        echo "---|                         !! No pak*.pk3 files found !!                        |---"
        echo "---| You need to upload all *.pk3 files from your 'main' and (optional) 'uo' game |---"
        echo "---| Please Upload and restart the container, putting server into sleep mode      |---"
        sleep infinity
    else
                echo "---| Serverfiles found, continuing |---"
fi

# Check if Punkbuster is enabled and if a config exist
if [ "${PUNKBUSTER}" == "1" ]; then
    echo "---| Punkbuster is enabled, copy binary legacy files |---"
    cp -r /lib/${USER}/v1.729 /gameserver/pb
    chmod -R 755 /gameserver/pb/
fi

if [ "${PUNKBUSTER}" == "1" ]; then
  if [ ! -f "/config/main/punkbuster.cfg" ]; then
    echo "---| Punkbuster is enabled but no punkbuster.cfg found in config dir |---"
    echo "---| Copying default punkbuster.cfg ... |---"
    mkdir /config/main && mkdir /config/uo
    cp /opt/config/punkbuster.cfg /config/main/punkbuster.cfg
    cp /opt/config/punkbuster.cfg /config/uo/punkbuster.cfg
   else
    echo "---| Punkbuster.cfg found. Activate Punkbuster in your dedicated.cfg or dedicateduo.cfg  |---"
  fi
   else
    echo "---| Punkbuster disabled, skipping  |---"
fi

# Start a Vanilla Call of Duty Server
if [ "${COD_GAME}" == "main" ]; then
    echo "---| Checking if the config file 'dedicated.cfg' for Call of Duty is present |---"
      if [ ! -f "/config/main/dedicated.cfg" ]; then
              echo "---|  No 'dedicated.cfg' for Call of Duty found, copying default dedicated.cfg |---"
          mkdir /config/main
          cp /opt/config/dedicated.cfg /config/main/dedicated.cfg
          echo "---|  Change 'dedicated.cfg to your needs and restart the container            |---"
          sleep infinity
        else
               echo "---| Call of Duty config 'dedicated.cfg' found, continuing |---"
          case ${COD_VERSION} in
                1_4)
                    echo "---| Start Vanilla CoD Server with Version 1.4 |---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;

                1_4_cracked)
                    echo "---| Start Vanilla CoD Server with Version 1.4 'Cracked'|---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;

                1_5)
                    echo "---| Start Vanilla CoD Server with Version 1.5 |---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;
                1_5_ext)
                    echo "---| Start Vanilla CoD Server with Version 1.5 with codextended |---"
                    cp /bin/cod_lnxded_1_5 /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    LD_PRELOAD=/bin/codextendedv5.so ./cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;
                1_5_cracked)
                    echo "---| Start Vanilla CoD Server with Version 1.5 'Cracked'|---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;

                1_5b)
                    echo "---| Start Vanilla CoD Server with Version 1.5b |---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;

                1_5b_cracked)
                    echo "---| Start Vanilla CoD Server with Version 1.5b 'Cracked' |---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/cod_lnxded
                    chmod +x cod_lnxded
                    cd /gameserver/
                    /gameserver/cod_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;
            esac
        fi

fi

# Start a vanilla Call of Duty: United Offensive Server
if [ "${COD_GAME}" == "uo" ]; then
    echo "---| Checking if the config file 'dedicateduo.cfg' for Call of Duty: United Offensive is present |---"
      if [ ! -f "/config/uo/dedicateduo.cfg" ]; then
              echo "---|  No 'dedicateduo.cfg' for Call of Duty: United Offensive found, copying default dedicateduo.cfg |---"
          mkdir /config/uo
          cp /opt/config/dedicateduo.cfg /config/uo/dedicateduo.cfg
          echo "---|  Change 'dedicateduo.cfg to your needs and restart the container                              |---"
          sleep infinity
        else
               echo "---| Call of Duty config 'dedicated.cfg' found, continuing |---"
          case ${COD_VERSION} in
                1_51)
                        echo "---| Start CoD: United Offensive Server with Version 1.51 |---"
                        cp /bin/coduo_lnxded_${COD_VERSION} /gameserver/coduo_lnxded
                        chmod +x coduo_lnxded
                        cd /gameserver/
                        /gameserver/coduo_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;

                1_51_cracked)
                        echo "---| Start CoD: United Offensive Server with Version 1.51 'Cracked' |---"
                    cp /bin/cod_lnxded_${COD_VERSION} /gameserver/coduo_lnxded
                        chmod +x coduo_lnxded
                        cd /gameserver/
                        /gameserver/coduo_lnxded +set fs_basepath /gameserver ${STARTUP}
                    ;;
            esac
        fi

fi
